import java.text.SimpleDateFormat;
import java.util.Date;

public class Cuenta{
 private int id;
 private double balance;
 private double tasaDeInteresAnual = 0;
 private Date fechaDeCreacion;
 
 public Cuenta(){
   this.id = 0;
   this.balance = 0;
   this.fechaDeCreacion.toString();
 }
 
 public Cuenta(int id, double balance, double tasaDeInteresAnual){
   this.id = id;
   this.balance = balance;
   this.fechaDeCreacion.toString();
 }
 
 public int getid(){
   return this.id;
 }
 
 public void setid(){
   this.id = id;
 }
 public double getbalance(){
   return this.balance;
 }
 
 public void setbalance(){
   this.balance = balance;
 }
 public double gettasaDeInteresAnual(){
   return this.tasaDeInteresAnual;
 }
 
 public void settasaDeInteresAnual(){
   this.tasaDeInteresAnual = tasaDeInteresAnual;
 }
 public String getfechaDeCreacion(){
   return this.fechaDeCreacion.toString();
 }
 public double obtenerTasaDeInteresMensual(){
   return tasaDeInteresAnual/12;
 }
 public double calcularInteresMensual(){
   return balance * tasaDeInteresAnual;
 }
 public double retirarDinero(double balance, double retirar){
   balance = balance - retirar;
   return balance;
 }
 public double depositarDinero(double balance, double depositar){
   balance = balance - depositar;
   return balance;
 }
}